import math as m

'''
Example 1:

Input: s = "42"
Output: 42
Since 42 is in the range [-231, 231 - 1], the final result is 42.

Example 2:

Input: s = "   -42"
Output: -42
Since -42 is in the range [-231, 231 - 1], the final result is -42.

Example 3:

Input: s = "4193 with words"
Output: 4193
Since 4193 is in the range [-231, 231 - 1], the final result is 4193.

Example 4:

Input: s = "words and 987"
Output: 0
Explanation:
The parsed integer is 0 because no digits were read.
Since 0 is in the range [-231, 231 - 1], the final result is 0.

Example 5:

Input: s = "-91283472332"
Output: -2147483648
Explanation:
The parsed integer is -91283472332.
Since -91283472332 is less than the lower bound of the range [-231, 231 - 1], the final result is clamped to -231 = -2147483648.
'''

class Solution:
    def myAtoi(self, s: str) -> int:
        result = 0
        isNegativeNumber = False

        s = s.lstrip() # remove left whitespace

        if(s == ""):
            return 0

        if(s[0] == '-'):
            if(len(s)>1 and s[1].isdigit()):
                isNegativeNumber = True
                s = s[1:]
            else:
                return 0

        if(s[0] == '+'):
            if(len(s)>1 and s[1].isdigit()):
                s = s[1:]
            else:
                return 0

        if(not(s[0].isdigit())):
            return 0

        for c in s.lstrip():
            if c.isdigit():
                result = result * 10 + int(c)
            else:
                break

        if isNegativeNumber:
            return -result if result < 2**31 else -2**31

        return result if result < 2**31 else 2**31 - 1

if __name__ == "__main__":
    solution = Solution()

    print("Solution")

    print("Input: 42")
    print("Output: " + str(solution.myAtoi("42")))

    print("Input:    -42")
    print("Output: " + str(solution.myAtoi("   -42")))

    print("Input: 4193 with words")
    print("Output: " + str(solution.myAtoi("4193 with words")))

    print("Input: words and 987")
    print("Output: " + str(solution.myAtoi("words and 987")))

    print("Input: -91283472332")
    print("Output: " + str(solution.myAtoi("-91283472332")))

    print("Input: 0032")
    print("Output: " + str(solution.myAtoi("0032")))

    print("Input: 3.14159")
    print("Output: " + str(solution.myAtoi("3.14159")))

    print("Input: +1")
    print("Output: " + str(solution.myAtoi("+1")))

    print("Input: +-12")
    print("Output: " + str(solution.myAtoi("+-12")))

    print("Input: -+12")
    print("Output: " + str(solution.myAtoi("-+12")))

    print("Input: 21474836460")
    print("Output: " + str(solution.myAtoi("21474836460")))