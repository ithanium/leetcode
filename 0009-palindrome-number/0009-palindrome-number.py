import math as m

'''
Given an integer x, return true if x is palindrome integer.

An integer is a palindrome when it reads the same backward 
as forward. For example, 121 is palindrome while 123 is not.
'''

class Solution:
    def isPalindrome(self, x: int) -> bool:
        if x < 0:
            return False
        
        x_copy = x
        x_reversed = 0
        
        while x_copy > 0:
            x_reversed = x_reversed * 10 + x_copy % 10
            x_copy = m.floor(x_copy / 10)
        
        return x == x_reversed

if __name__ == "__main__":
    solution = Solution()

    print("Solution")

    print("Input: 121")
    print("Output: " + str(solution.isPalindrome(121)))
