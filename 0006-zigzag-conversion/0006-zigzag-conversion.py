'''
The string "PAYPALISHIRING" is written in a zigzag pattern
on a given number of rows like this: (you may want to
display this pattern in a fixed font for better legibility)

P   A   H   N
A P L S I I G
Y   I   R

Example 1:
Input: s = "PAYPALISHIRING", numRows = 3
Output: "PAHNAPLSIIGYIR"

Example 2:
Input: s = "PAYPALISHIRING", numRows = 4
Output: "PINALSIGYAHRPI"

Example 3:
Input: s = "A", numRows = 1
Output: "A"
'''

'''
Approach 1

PAYPALISHIRING

0 [PA] -> [P, A]
1 [APL] -> [A, P, L]
2 [Y] -> [Y]]

Time O(n + k < n + n = 2n) = O(n)
Space O(n)

n is the number of chracters in string
k is the numRows
'''

class SolutionOne(object):
    def convert(self, s, numRows):
        if numRows == 1 or numRows >= len(s):
            return s

        delta = -1
        row = 0
        res = [[] for i in range(numRows)]

        # iterate through string
        for c in s:
            res[row].append(c)
            if row == 0 or row == numRows - 1:
                delta *= -1
            row += delta

        # consolidate the result
        for i in range(len(res)):
            res[i] = ''.join(res[i])

        return ''.join(res)

'''
Approach 2

PAYPALISHIRING
row 0: 0 6 12      or 0 +6 +6
row 1: 1 5 7 11 13 or 1 +4 +2 +4 +2
row 2: 2 4 8 10    or 2 +2 +4 +2 +4
row 3: 3 9         or 3 +6 +6

P Y A
A P L

P   A
A P
Y

cycle = 2 * numRows - 2
1 rows -> 1 special case
2 rows -> 2
3 rows -> 4
4 rows -> 6

Time O(n)
Space O(n)

n is the number of characters in the string
'''

class SolutionTwo(object):
    def convert(self, s, numRows):
        if numRows == 1:
            return s
        
        cycle = 2 * numRows -2

        res = []
        for i in range(numRows):
            for j in range(i, len(s), cycle):
                res.append(s[j])

                # have to capture the element on the
                # zigzag diagonal
                k = j + cycle - 2 * i
                if i != 0 and i != numRows - 1 and k < len(s):
                    res.append(s[k])

        return ''.join(res)

if __name__ == "__main__":
    solutionOne = SolutionOne()
    solutionTwo = SolutionTwo()

    print("SolutionOne")
    print("Input: PAYPALISHIRING")
    print("Output: " + solutionOne.convert("PAYPALISHIRING", 3))

    print("SolutionTwo")
    print("Input: PAYPALISHIRING")
    print("Output: " + solutionTwo.convert("PAYPALISHIRING", 3))
