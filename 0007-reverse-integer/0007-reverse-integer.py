import math as m

class Solution:
    def reverse(self, x: int) -> int:
        if x < 0:
            return -self.reverse(-x)

        result = 0
        while x:
            result = result * 10 + x % 10
            x = m.floor(x / 10)
        return result if result <= 0x7fffffff else 0

if __name__ == "__main__":
    solution = Solution()

    print("Solution")
    print("Input: 123")
    print("Output: " + str(solution.reverse(123)))

    print("Input: -123")
    print("Output: " + str(solution.reverse(-123)))