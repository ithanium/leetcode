'''
Given a string s, find the length of the longest substring without repeating characters.

Example 1:
Input: s = "abcabcbb"
Output: 3
Explanation: The answer is "abc", with the length of 3.

Example 2:
Input: s = "bbbbb"
Output: 1
Explanation: The answer is "b", with the length of 1.

Example 3:
Input: s = "pwwkew"
Output: 3
Explanation: The answer is "wke", with the length of 3.
Notice that the answer must be a substring, "pwke" is a subsequence and not a substring.

Example 4:
Input: s = ""
Output: 0
'''

class Solution(object):
  def findLongestSubstringWithoutRepeatingCharacters(self, s):
    a_pointer = 0 
    b_pointer = 0
    maximum = 0

    hash_set = set()

    while(b_pointer < len(s)):
      char_at_a_pointer = s[a_pointer]
      char_at_b_pointer = s[b_pointer]

      if (not(char_at_b_pointer in hash_set)):
        hash_set.add(char_at_b_pointer)
        b_pointer = b_pointer + 1
        maximum = max(len(hash_set), maximum)
      else:
        hash_set.remove(char_at_a_pointer)
        a_pointer = a_pointer + 1 

    return maximum

if __name__ == "__main__":
    solution = Solution()

    print("Solution")

    print("Input: abcabcbb")
    print("Output: " + str(solution.findLongestSubstringWithoutRepeatingCharacters("abcabcbb")))

    print("Input: bbbbb")
    print("Output: " + str(solution.findLongestSubstringWithoutRepeatingCharacters("bbbbb")))

    print("Input: pwwkew")
    print("Output: " + str(solution.findLongestSubstringWithoutRepeatingCharacters("pwwkew")))

    print("Input: N/A")
    print("Output: " + str(solution.findLongestSubstringWithoutRepeatingCharacters("")))
