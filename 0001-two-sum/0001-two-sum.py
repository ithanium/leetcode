"""
Given an array of integers nums and an integer target, return indices of the two numbers such that they add up to target.

You may assume that each input would have exactly one solution, and you may not use the same element twice.

You can return the answer in any order.
"""
from typing import List


class Solution:
    def twoSum(self, nums: List[int], target: int) -> List[int]:
        prevMap = {}  # value : index

        for i, n in enumerate(nums):
            diff = target - n
            if diff in prevMap:
                return [prevMap[diff], i]
            prevMap[n] = i
        return


if __name__ == "__main__":
    solution = Solution()

    print("Solution")

    print("Input: nums = [2,7,11,15], target = 9")
    print("Output: " + str(solution.twoSum([2, 7, 11, 15], 9)))  # [0, 1]

    print("Input: nums = [3,2,4], target = 6")
    print("Output: " + str(solution.twoSum([3, 2, 4], 6)))  # [1, 2]

    print("Input: nums = [3,3], target = 6")
    print("Output: " + str(solution.twoSum([3, 3], 6)))  # [0, 1]
