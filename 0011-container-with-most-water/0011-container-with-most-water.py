import math as m
from typing import List

'''
Given n non-negative integers a1, a2, ..., an , where each represents 
a point at coordinate (i, ai). n vertical lines are drawn such that 
the two endpoints of the line i is at (i, ai) and (i, 0). Find two 
lines, which, together with the x-axis forms a container, such that 
the container contains the most water.
'''

class Solution:
    def maxArea(self, height: List[int]) -> int:
        maxArea = 0
        left = 0
        right = len(height) - 1

        while(left < right):
            area = min(height[left], height[right]) * (right - left)
            maxArea = max(maxArea, area)
            if(height[left] < height[right]):
                left = left + 1
            else:
                right = right - 1

        return maxArea


if __name__ == "__main__":
    solution = Solution()

    print("Solution")

    print("Input: height = [1,8,6,2,5,4,8,3,7]")
    print("Output: " + str(solution.maxArea([1,8,6,2,5,4,8,3,7])))
